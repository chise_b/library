package com.project.library.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Book.class)
public abstract class Book_ {

	public static volatile SingularAttribute<Book, Integer> yearOfPublishing;
	public static volatile SingularAttribute<Book, String> author;
	public static volatile SetAttribute<Book, Genre> genres;
	public static volatile SingularAttribute<Book, String> publishingHouse;
	public static volatile SingularAttribute<Book, Long> id;
	public static volatile SingularAttribute<Book, String> title;

	public static final String YEAR_OF_PUBLISHING = "yearOfPublishing";
	public static final String AUTHOR = "author";
	public static final String GENRES = "genres";
	public static final String PUBLISHING_HOUSE = "publishingHouse";
	public static final String ID = "id";
	public static final String TITLE = "title";

}

