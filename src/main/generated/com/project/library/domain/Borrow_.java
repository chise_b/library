package com.project.library.domain;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Borrow.class)
public abstract class Borrow_ {

	public static volatile SingularAttribute<Borrow, Boolean> approved;
	public static volatile SingularAttribute<Borrow, LocalDate> endDate;
	public static volatile SingularAttribute<Borrow, Book> book;
	public static volatile SingularAttribute<Borrow, Long> id;
	public static volatile SingularAttribute<Borrow, User> user;
	public static volatile SingularAttribute<Borrow, LocalDate> startDate;

	public static final String APPROVED = "approved";
	public static final String END_DATE = "endDate";
	public static final String BOOK = "book";
	public static final String ID = "id";
	public static final String USER = "user";
	public static final String START_DATE = "startDate";

}

