package com.project.library.repository;
import com.project.library.domain.Borrow;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Borrow entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BorrowRepository extends JpaRepository<Borrow, Long> {

    @Query("select borrow from Borrow borrow where borrow.user.login = ?#{principal.username}")
    List<Borrow> findByUserIsCurrentUser();

    @Query("select borrow from Borrow borrow where borrow.book.id= :bookId and borrow.startDate < :startDate and borrow.endDate > :startDate and " +
        "borrow.approved = 1" )
    Optional<Borrow> findByBookAndBorrowInterval(@Param("bookId") Long bookId, @Param("startDate") LocalDate startDate);

}
