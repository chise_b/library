/**
 * View Models used by Spring MVC REST controllers.
 */
package com.project.library.web.rest.vm;
