package com.project.library.web.rest;

import com.project.library.domain.Authority;
import com.project.library.domain.Borrow;
import com.project.library.domain.User;
import com.project.library.repository.BorrowRepository;
import com.project.library.security.AuthoritiesConstants;
import com.project.library.service.MailService;
import com.project.library.service.UserService;
import com.project.library.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing {@link com.project.library.domain.Borrow}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BorrowResource {

    private final Logger log = LoggerFactory.getLogger(BorrowResource.class);

    private static final String ENTITY_NAME = "borrow";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MailService mailService;

    private final UserService userService;

    private final BorrowRepository borrowRepository;

    public BorrowResource(BorrowRepository borrowRepository, MailService mailService, UserService userService) {
        this.borrowRepository = borrowRepository;
        this.mailService = mailService;
        this.userService = userService;
    }

    /**
     * {@code POST  /borrows} : Create a new borrow.
     *
     * @param borrow the borrow to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new borrow, or with status {@code 400 (Bad Request)} if the borrow has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/borrows")
    public ResponseEntity<Borrow> createBorrow(@RequestBody Borrow borrow) throws URISyntaxException {
        log.debug("REST request to save Borrow : {}", borrow);
        if (borrow.getId() != null) {
            throw new BadRequestAlertException("A new borrow cannot already have an ID", ENTITY_NAME, "idexists");
        }
        validateBorrow(borrow);
        Borrow result = borrowRepository.save(borrow);
        return ResponseEntity.created(new URI("/api/borrows/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * Validate a borrow.
     *
     * @param borrow the borrow to be validated.
     * @throws BadRequestAlertException if the borrow is invalid (the requested book is unavailable on the specified date interval)
     */
    private void validateBorrow(@RequestBody Borrow borrow) {
        Borrow existingBorrow = borrowRepository.findByBookAndBorrowInterval(borrow.getBook().getId(),
            borrow.getStartDate()).orElse(null);
        if (Objects.nonNull(existingBorrow)) {
            throw new BadRequestAlertException("The requested book is unavailable on the specified date interval.", ENTITY_NAME, "unavailable");
        }
    }

    /**
     * {@code PUT  /borrows} : Updates an existing borrow.
     *
     * @param borrow the borrow to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated borrow,
     * or with status {@code 400 (Bad Request)} if the borrow is not valid,
     * or with status {@code 500 (Internal Server Error)} if the borrow couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/borrows")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Borrow> updateBorrow(@RequestBody Borrow borrow) throws URISyntaxException {
        log.debug("REST request to update Borrow : {}", borrow);
        if (borrow.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Boolean currentStatus = null;
        Optional<Borrow> optionalCurrentBorrow = borrowRepository.findById(borrow.getId());
        if(optionalCurrentBorrow.isPresent()){
            Borrow currentBorrow = optionalCurrentBorrow.get();
            currentStatus = currentBorrow.isApproved();
        }

        Borrow result = borrowRepository.save(borrow);
        if(currentStatus!=null && currentStatus!=borrow.isApproved()){
                mailService.sendApproveDisapproveMail(result);
        }

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, borrow.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /borrows} : get all the borrows.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of borrows in body.
     */
    @GetMapping("/borrows")
    public ResponseEntity<List<Borrow>> getAllBorrows(Pageable pageable) {
        log.debug("REST request to get a page of Borrows");
        Page<Borrow> page = borrowRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        User user = userService.getUserWithAuthorities().get();
        Set<Authority> authoritySet = user.getAuthorities();

        boolean isAdmin = false;
        for(Authority auth : authoritySet){
            if(auth.getName().equals(AuthoritiesConstants.ADMIN)){
                isAdmin = true;
                break;
            }
        }
        if(!isAdmin){
            List<Borrow> borrows = borrowRepository.findByUserIsCurrentUser();
            return ResponseEntity.ok().body(borrows);

        }

        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /borrows/:id} : get the "id" borrow.
     *
     * @param id the id of the borrow to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the borrow, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/borrows/{id}")
    public ResponseEntity<Borrow> getBorrow(@PathVariable Long id) {
        log.debug("REST request to get Borrow : {}", id);
        Optional<Borrow> borrow = borrowRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(borrow);
    }

    /**
     * {@code DELETE  /borrows/:id} : delete the "id" borrow.
     *
     * @param id the id of the borrow to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/borrows/{id}")
    @PreAuthorize("hasRole(\"" + AuthoritiesConstants.ADMIN + "\")")
    public ResponseEntity<Void> deleteBorrow(@PathVariable Long id) {
        log.debug("REST request to delete Borrow : {}", id);
        borrowRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
