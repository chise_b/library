import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { IBook } from 'app/shared/model/book.model';
import { getEntities as getBooks } from 'app/entities/book/book.reducer';
import { getEntity, updateEntity, createEntity, reset } from './borrow.reducer';
import { IBorrow } from 'app/shared/model/borrow.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import {hasAnyAuthority} from "app/shared/auth/private-route";
import {AUTHORITIES} from "app/config/constants";

export interface IBorrowUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IBorrowUpdateState {
  isNew: boolean;
  userId: string;
  bookId: string;
}

export class BorrowUpdate extends React.Component<IBorrowUpdateProps, IBorrowUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      userId: '0',
      bookId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getUsers();
    this.props.getBooks();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { borrowEntity } = this.props;
      const entity = {
        ...borrowEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/borrow');
  };

  render() {
    const { borrowEntity, users, books, loading, updating, isAdmin } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="projectApp.borrow.home.createOrEditLabel">Create or edit a Borrow</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : borrowEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="borrow-id">ID</Label>
                    <AvInput id="borrow-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="startDateLabel" for="borrow-startDate">
                    Start Date
                  </Label>
                  <AvField id="borrow-startDate" type="date" className="form-control" name="startDate" />
                </AvGroup>
                <AvGroup>
                  <Label id="endDateLabel" for="borrow-endDate">
                    End Date
                  </Label>
                  <AvField id="borrow-endDate" type="date" className="form-control" name="endDate" />
                </AvGroup>
                <AvGroup>
                  <Label id="approvedLabel" check>
                    <AvInput id="borrow-approved" type="checkbox" className="form-control" name="approved" disabled={!isAdmin} />
                    Approved
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label for="borrow-user">User</Label>
                  <AvInput id="borrow-user" type="select" className="form-control" name="user.id">
                    <option value="" key="0" />
                    {users
                      ? users.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.login}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="borrow-book">Book</Label>
                  <AvInput id="borrow-book" type="select" className="form-control" name="book.id">
                    <option value="" key="0" />
                    {books
                      ? books.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.title}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/borrow" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  users: storeState.userManagement.users,
  books: storeState.book.entities,
  borrowEntity: storeState.borrow.entity,
  loading: storeState.borrow.loading,
  updating: storeState.borrow.updating,
  updateSuccess: storeState.borrow.updateSuccess,
  isAdmin:hasAnyAuthority(storeState.authentication.account.authorities, [AUTHORITIES.ADMIN])
});
const mapDispatchToProps = {
  getUsers,
  getBooks,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BorrowUpdate);
