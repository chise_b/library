import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './borrow.reducer';
import { IBorrow } from 'app/shared/model/borrow.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
import { AUTHORITIES } from 'app/config/constants';
import {hasAnyAuthority} from "app/shared/auth/private-route";

export interface IBorrowProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IBorrowState = IPaginationBaseState;

export class Borrow extends React.Component<IBorrowProps, IBorrowState> {
  state: IBorrowState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { borrowList, match, totalItems, isAdmin } = this.props;
    return (
      <div>
        <h2 id="borrow-heading">
          Borrows
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create a new Borrow
          </Link>
        </h2>
        <div className="table-responsive">
          {borrowList && borrowList.length > 0 ? (
            <Table responsive aria-describedby="borrow-heading">
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    ID <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('startDate')}>
                    Start Date <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('endDate')}>
                    End Date <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('approved')}>
                    Approved <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    User <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    Book <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {borrowList.map((borrow, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${borrow.id}`} color="link" size="sm">
                        {borrow.id}
                      </Button>
                    </td>
                    <td>
                      <TextFormat type="date" value={borrow.startDate} format={APP_LOCAL_DATE_FORMAT} />
                    </td>
                    <td>
                      <TextFormat type="date" value={borrow.endDate} format={APP_LOCAL_DATE_FORMAT} />
                    </td>
                    <td>{borrow.approved ? 'true' : 'false'}</td>
                    <td>{borrow.user ? borrow.user.login : ''}</td>
                    <td>{borrow.book ? <Link to={`book/${borrow.book.id}`}>{borrow.book.title}</Link> : ''}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${borrow.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                        </Button>

                          <Button tag={Link} to={`${match.url}/${borrow.id}/edit`} color="primary" size="sm"
                            >
                              <FontAwesomeIcon icon="pencil-alt"/> <span className="d-none d-md-inline">Edit</span>
                            </Button>


                          < Button tag = {Link} to={`${match.url}/${borrow.id}/delete`} color="danger" size="sm"
                          >
                          <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                          </Button>

                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">No Borrows found</div>
          )}
        </div>
        {isAdmin ?
          (<div className={borrowList && borrowList.length > 0 ? '' : 'd-none'}>
            <Row className="justify-content-center">
              <JhiItemCount page={this.state.activePage} total={totalItems} itemsPerPage={this.state.itemsPerPage} />
            </Row>
            <Row className="justify-content-center">
              <JhiPagination
                activePage={this.state.activePage}
                onSelect={this.handlePagination}
                maxButtons={5}
                itemsPerPage={this.state.itemsPerPage}
                totalItems={this.props.totalItems}
              />
            </Row>
          </div>)
          :
          null
        }
      </div>
    );
  }
}

const mapStateToProps = ({ borrow, authentication }: IRootState) => ({
  borrowList: borrow.entities,
  totalItems: borrow.totalItems,
  isAdmin:hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN])
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Borrow);
