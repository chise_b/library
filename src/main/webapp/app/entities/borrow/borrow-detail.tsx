import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './borrow.reducer';
import { IBorrow } from 'app/shared/model/borrow.model';
import {APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT, AUTHORITIES} from 'app/config/constants';
import {hasAnyAuthority} from "app/shared/auth/private-route";

export interface IBorrowDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class BorrowDetail extends React.Component<IBorrowDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { borrowEntity, isAdmin } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Borrow [<b>{borrowEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="startDate">Start Date</span>
            </dt>
            <dd>
              <TextFormat value={borrowEntity.startDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="endDate">End Date</span>
            </dt>
            <dd>
              <TextFormat value={borrowEntity.endDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="approved">Approved</span>
            </dt>
            <dd>{borrowEntity.approved ? 'true' : 'false'}</dd>
            <dt>User</dt>
            <dd>{borrowEntity.user ? borrowEntity.user.login : ''}</dd>
            <dt>Book</dt>
            <dd>{borrowEntity.book ? borrowEntity.book.title : ''}</dd>
          </dl>
          <Button tag={Link} to="/borrow" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;

            <Button tag={Link} to={`/borrow/${borrowEntity.id}/edit`} replace color="primary">
                <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
              </Button>


        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ borrow, authentication }: IRootState) => ({
  borrowEntity: borrow.entity,
  isAdmin:hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN])
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BorrowDetail);
