import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './genre.reducer';
import { IGenre } from 'app/shared/model/genre.model';
import {APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT, AUTHORITIES} from 'app/config/constants';
import {hasAnyAuthority} from "app/shared/auth/private-route";

export interface IGenreDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class GenreDetail extends React.Component<IGenreDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { genreEntity, isAdmin } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Genre [<b>{genreEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">Name</span>
            </dt>
            <dd>{genreEntity.name}</dd>
          </dl>
          <Button tag={Link} to="/genre" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          {isAdmin ?
            (<Button tag={Link} to={`/genre/${genreEntity.id}/edit`} replace color="primary">
              <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
            </Button>)
            :
            null
          }
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ genre, authentication }: IRootState) => ({
  genreEntity: genre.entity,
  isAdmin:hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN])
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GenreDetail);
