import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './genre.reducer';
import { IGenre } from 'app/shared/model/genre.model';
import {APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT, AUTHORITIES} from 'app/config/constants';
import {hasAnyAuthority} from "app/shared/auth/private-route";

export interface IGenreProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Genre extends React.Component<IGenreProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { genreList, match, isAdmin } = this.props;
    return (
      <div>
        <h2 id="genre-heading">
          Genres
          {
            isAdmin ?
              <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
                <FontAwesomeIcon icon="plus" />
                &nbsp; Create a new Genre
              </Link>
              :
              null
          }

        </h2>
        <div className="table-responsive">
          {genreList && genreList.length > 0 ? (
            <Table responsive aria-describedby="genre-heading">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {genreList.map((genre, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${genre.id}`} color="link" size="sm">
                        {genre.id}
                      </Button>
                    </td>
                    <td>{genre.name}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${genre.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                        </Button>
                        {
                          isAdmin ?
                            (<Button tag={Link} to={`${match.url}/${genre.id}/edit`} color="primary" size="sm">
                              <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                            </Button>)
                            :
                            null
                        }
                        {
                          isAdmin ?
                            (<Button tag={Link} to={`${match.url}/${genre.id}/delete`} color="danger" size="sm">
                              <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                            </Button>)
                            :
                            null
                        }
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">No Genres found</div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ genre, authentication }: IRootState) => ({
  genreList: genre.entities,
  isAdmin:hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN])
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Genre);
