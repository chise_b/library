import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './book.reducer';
import { IBook } from 'app/shared/model/book.model';
import {APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT, AUTHORITIES} from 'app/config/constants';
import {hasAnyAuthority} from "app/shared/auth/private-route";

export interface IBookDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class BookDetail extends React.Component<IBookDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { bookEntity, isAdmin } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Book [<b>{bookEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="author">Author</span>
            </dt>
            <dd>{bookEntity.author}</dd>
            <dt>
              <span id="title">Title</span>
            </dt>
            <dd>{bookEntity.title}</dd>
            <dt>
              <span id="publishingHouse">Publishing House</span>
            </dt>
            <dd>{bookEntity.publishingHouse}</dd>
            <dt>
              <span id="yearOfPublishing">Year Of Publishing</span>
            </dt>
            <dd>{bookEntity.yearOfPublishing}</dd>
            <dt>Genres</dt>
            <dd>
              {bookEntity.genres
                ? bookEntity.genres.map((val, i) => (
                    <span key={val.id}>
                      <a>{val.name}</a>
                      {i === bookEntity.genres.length - 1 ? '' : ', '}
                    </span>
                  ))
                : null}
            </dd>
          </dl>
          <Button tag={Link} to="/book" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          {isAdmin ?
            (<Button tag={Link} to={`/book/${bookEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>)
            :
            null
          }
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ book, authentication }: IRootState) => ({
  bookEntity: book.entity,
  isAdmin:hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN])
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookDetail);
