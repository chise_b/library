import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IGenre } from 'app/shared/model/genre.model';
import { getEntities as getGenres } from 'app/entities/genre/genre.reducer';
import { getEntity, updateEntity, createEntity, reset } from './book.reducer';
import { IBook } from 'app/shared/model/book.model';
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IBookUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IBookUpdateState {
  isNew: boolean;
  idsgenres: any[];
}

export class BookUpdate extends React.Component<IBookUpdateProps, IBookUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      idsgenres: [],
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getGenres();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { bookEntity } = this.props;
      const entity = {
        ...bookEntity,
        ...values,
        genres: mapIdList(values.genres)
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/book');
  };

  render() {
    const { bookEntity, genres, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="projectApp.book.home.createOrEditLabel">Create or edit a Book</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : bookEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="book-id">ID</Label>
                    <AvInput id="book-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="authorLabel" for="book-author">
                    Author
                  </Label>
                  <AvField id="book-author" type="text" name="author" />
                </AvGroup>
                <AvGroup>
                  <Label id="titleLabel" for="book-title">
                    Title
                  </Label>
                  <AvField
                    id="book-title"
                    type="text"
                    name="title"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="publishingHouseLabel" for="book-publishingHouse">
                    Publishing House
                  </Label>
                  <AvField id="book-publishingHouse" type="text" name="publishingHouse" />
                </AvGroup>
                <AvGroup>
                  <Label id="yearOfPublishingLabel" for="book-yearOfPublishing">
                    Year Of Publishing
                  </Label>
                  <AvField id="book-yearOfPublishing" type="string" className="form-control" name="yearOfPublishing" />
                </AvGroup>
                <AvGroup>
                  <Label for="book-genres">Genres</Label>
                  <AvInput
                    id="book-genres"
                    type="select"
                    multiple
                    className="form-control"
                    name="genres"
                    value={bookEntity.genres && bookEntity.genres.map(e => e.id)}
                  >
                    <option value="" key="0" />
                    {genres
                      ? genres.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/book" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  genres: storeState.genre.entities,
  bookEntity: storeState.book.entity,
  loading: storeState.book.loading,
  updating: storeState.book.updating,
  updateSuccess: storeState.book.updateSuccess
});

const mapDispatchToProps = {
  getGenres,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookUpdate);
