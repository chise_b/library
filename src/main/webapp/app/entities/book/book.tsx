import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './book.reducer';
import { IBook } from 'app/shared/model/book.model';
import {APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT, AUTHORITIES} from 'app/config/constants';
import {hasAnyAuthority} from "app/shared/auth/private-route";
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';
export interface IBookProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}
export type IBookState = IPaginationBaseState;

export class Book extends React.Component<IBookProps, IBookState> {
  state: IBookState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.props.getEntities();
  }

  sort = prop => () => {
      this.setState(
        {
          order: this.state.order === 'asc' ? 'desc' : 'asc',
          sort: prop
        },
        () => this.sortEntities()
      );
    };

  sortEntities() {
      this.getEntities();
      this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
    }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());
  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { bookList, match, totalItems, isAdmin } = this.props;
    return (
      <div>
        <h2 id="book-heading">
          Books
          {
            isAdmin ?
            (<Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
              <FontAwesomeIcon icon="plus" />
              &nbsp; Create a new Book
            </Link>)
            :
            null
          }

        </h2>
        <div className="table-responsive">
          {bookList && bookList.length > 0 ? (
            <Table responsive aria-describedby="book-heading">
              <thead>
                <tr>
                <th className="hand" onClick={this.sort('id')}>
                  ID <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('author')}>
                  Author <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('title')}>
                  Title <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('publishingHouse')}>
                  Publishing House <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('yearOfPublishing')}>
                  Year Of Publishing <FontAwesomeIcon icon="sort" />
                </th>
                <th >
                  Genres
                </th>
                </tr>
              </thead>
              <tbody>
                {bookList.map((book, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${book.id}`} color="link" size="sm">
                        {book.id}
                      </Button>
                    </td>
                    <td>{book.author}</td>
                    <td>{book.title}</td>
                    <td>{book.publishingHouse}</td>
                    <td>{book.yearOfPublishing}</td>
                    <td>
                      {book.genres
                        ? book.genres.map((val, j) => (
                            <span key={j}>
                              <Link to={`genre/${val.id}`}>{val.name}</Link>
                              {j === book.genres.length - 1 ? '' : ', '}
                            </span>
                          ))
                        : null}
                    </td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${book.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                        </Button>
                        {isAdmin ?
                          (<Button tag={Link} to={`${match.url}/${book.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                        </Button>)
                          :
                          null}
                        { isAdmin ?
                          (<Button tag={Link} to={`${match.url}/${book.id}/delete`} color="danger" size="sm">
                            <FontAwesomeIcon icon="trash"/> <span className="d-none d-md-inline">Delete</span>
                          </Button>)
                          :
                          null
                        }
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">No Books found</div>
          )}
        </div>
        <div className={bookList && bookList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={this.state.activePage} total={totalItems} itemsPerPage={this.state.itemsPerPage} />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={this.state.activePage}
              onSelect={this.handlePagination}
              maxButtons={5}
              itemsPerPage={this.state.itemsPerPage}
              totalItems={this.props.totalItems}
            />
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ book, authentication }: IRootState) => ({
  bookList: book.entities,
  totalItems: book.totalItems,
  isAdmin:hasAnyAuthority(authentication.account.authorities, [AUTHORITIES.ADMIN])
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Book);
