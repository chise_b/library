import { IGenre } from 'app/shared/model/genre.model';

export interface IBook {
  id?: number;
  author?: string;
  title?: string;
  publishingHouse?: string;
  yearOfPublishing?: number;
  genres?: IGenre[];
}

export const defaultValue: Readonly<IBook> = {};
