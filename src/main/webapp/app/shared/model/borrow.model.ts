import { Moment } from 'moment';
import { IUser } from 'app/shared/model/user.model';
import { IBook } from 'app/shared/model/book.model';

export interface IBorrow {
  id?: number;
  startDate?: Moment;
  endDate?: Moment;
  approved?: boolean;
  user?: IUser;
  book?: IBook;
}

export const defaultValue: Readonly<IBorrow> = {
  approved: false
};
