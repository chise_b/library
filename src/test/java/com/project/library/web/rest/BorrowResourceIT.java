package com.project.library.web.rest;

import com.project.library.ProjectApp;
import com.project.library.domain.Borrow;
import com.project.library.repository.BorrowRepository;
import com.project.library.service.MailService;
import com.project.library.service.UserService;
import com.project.library.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.project.library.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BorrowResource} REST controller.
 */
@SpringBootTest(classes = ProjectApp.class)
public class BorrowResourceIT {

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_APPROVED = false;
    private static final Boolean UPDATED_APPROVED = true;

    @Autowired
    private BorrowRepository borrowRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private MailService mailService;

    @Autowired
    private UserService userService;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBorrowMockMvc;

    private Borrow borrow;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BorrowResource borrowResource = new BorrowResource(borrowRepository, mailService, userService);
        this.restBorrowMockMvc = MockMvcBuilders.standaloneSetup(borrowResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Borrow createEntity(EntityManager em) {
        Borrow borrow = new Borrow()
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .approved(DEFAULT_APPROVED);
        return borrow;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Borrow createUpdatedEntity(EntityManager em) {
        Borrow borrow = new Borrow()
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .approved(UPDATED_APPROVED);
        return borrow;
    }

    @BeforeEach
    public void initTest() {
        borrow = createEntity(em);
    }

    @Test
    @Transactional
    public void createBorrow() throws Exception {
        int databaseSizeBeforeCreate = borrowRepository.findAll().size();

        // Create the Borrow
        restBorrowMockMvc.perform(post("/api/borrows")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(borrow)))
            .andExpect(status().isCreated());

        // Validate the Borrow in the database
        List<Borrow> borrowList = borrowRepository.findAll();
        assertThat(borrowList).hasSize(databaseSizeBeforeCreate + 1);
        Borrow testBorrow = borrowList.get(borrowList.size() - 1);
        assertThat(testBorrow.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testBorrow.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testBorrow.isApproved()).isEqualTo(DEFAULT_APPROVED);
    }

    @Test
    @Transactional
    public void createBorrowWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = borrowRepository.findAll().size();

        // Create the Borrow with an existing ID
        borrow.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBorrowMockMvc.perform(post("/api/borrows")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(borrow)))
            .andExpect(status().isBadRequest());

        // Validate the Borrow in the database
        List<Borrow> borrowList = borrowRepository.findAll();
        assertThat(borrowList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBorrows() throws Exception {
        // Initialize the database
        borrowRepository.saveAndFlush(borrow);

        // Get all the borrowList
        restBorrowMockMvc.perform(get("/api/borrows?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(borrow.getId().intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].approved").value(hasItem(DEFAULT_APPROVED.booleanValue())));
    }

    @Test
    @Transactional
    public void getBorrow() throws Exception {
        // Initialize the database
        borrowRepository.saveAndFlush(borrow);

        // Get the borrow
        restBorrowMockMvc.perform(get("/api/borrows/{id}", borrow.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(borrow.getId().intValue()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.approved").value(DEFAULT_APPROVED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingBorrow() throws Exception {
        // Get the borrow
        restBorrowMockMvc.perform(get("/api/borrows/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBorrow() throws Exception {
        // Initialize the database
        borrowRepository.saveAndFlush(borrow);

        int databaseSizeBeforeUpdate = borrowRepository.findAll().size();

        // Update the borrow
        Borrow updatedBorrow = borrowRepository.findById(borrow.getId()).get();
        // Disconnect from session so that the updates on updatedBorrow are not directly saved in db
        em.detach(updatedBorrow);
        updatedBorrow
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .approved(UPDATED_APPROVED);

        restBorrowMockMvc.perform(put("/api/borrows")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBorrow)))
            .andExpect(status().isOk());

        // Validate the Borrow in the database
        List<Borrow> borrowList = borrowRepository.findAll();
        assertThat(borrowList).hasSize(databaseSizeBeforeUpdate);
        Borrow testBorrow = borrowList.get(borrowList.size() - 1);
        assertThat(testBorrow.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testBorrow.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testBorrow.isApproved()).isEqualTo(UPDATED_APPROVED);
    }

    @Test
    @Transactional
    public void updateNonExistingBorrow() throws Exception {
        int databaseSizeBeforeUpdate = borrowRepository.findAll().size();

        // Create the Borrow

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBorrowMockMvc.perform(put("/api/borrows")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(borrow)))
            .andExpect(status().isBadRequest());

        // Validate the Borrow in the database
        List<Borrow> borrowList = borrowRepository.findAll();
        assertThat(borrowList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBorrow() throws Exception {
        // Initialize the database
        borrowRepository.saveAndFlush(borrow);

        int databaseSizeBeforeDelete = borrowRepository.findAll().size();

        // Delete the borrow
        restBorrowMockMvc.perform(delete("/api/borrows/{id}", borrow.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Borrow> borrowList = borrowRepository.findAll();
        assertThat(borrowList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
